# rust_cid_npm

**Fast and tiny rust library, CLI tool, and npm package to generate CIDs without a full IPFS client**

## Overview

This repo contains a minimal rust library, a CLI application, and wasm binding exposed in an npm package to generate [IPFS](https://ipfs.io) CIDs of a given file. It aims to be _fast_ to execute and _tiny_ on the wire and at rest.

The intent of the tools is to be for CID generation/verification _only_. It is not in any way an IPFS client, so when you don't need all of [rust-ipfs](https://github.com/rs-ipfs/rust-ipfs) or [js-ipfs](https://github.com/ipfs/js-ipfs) 

**Exampe use case:** you want is to genereate a CID of a file on your (web)app to cross-check it's validity, an/or broadcast this to a full IPFS client. 

The intent of the CLI tool is to use as a dev tool, and (eventually) target embeded devices that want to interact with files on IPFS (verification, and chunking), but not be a full cient. 

## Examples

### Webapp using npm

See the `./node-app` for an example application implimenting the library with `webpack`.

```bash
cd node-app
npm install
npm start
```

### CLI tool

The code can be found at `./src/bin/main.rs`. 

To compile and run this executable:

```bash
cargo build
cargo run # put your file name here<filename>
cargo run test.txt
```


## Development

Build an `npm` ready package with `wasm-pack build`.
Learn about [`wasm-pack` here](https://github.com/rustwasm/wasm-pack)

At the moment, `./pkg/package.json` is overwritten each time build is run. You need to manually overwrite this with `./package.json` after the build.

### Testing

Tests are in `./src/lib.rs` for the library, `./src/bin/main.rs` for the cli, and `./tests/web.rs` for browser integrations.

```bash
#tests for raw library and cli tests
cargo test 
#tests for web browsers
wasm-pack test --chrome # or --firefox or --safari then open the browser to check tests on the generated page
```

## Future Work

- [ ] More tests, examples
- [ ] Extent CLI functionality
    - JSON output to stdout and/or file
    - Options/Flags:
        - [ ] Output type & destination
        - [ ] Input Multiple files, output 
        - [ ] Input Directories
- [ ] Target embeded devices and test 
    - [ ] ARM devices (raspi, arduino)
    - [ ] IoT devices